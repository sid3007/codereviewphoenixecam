AFRAME.registerComponent('enable-clip', {
  init: function() {
    var sceneEl = this.el;
    var renderer = sceneEl.renderer;
    renderer.localClippingEnabled = true;
  }
})