window.onload = function()
{
	var url = document.location.href,
	params = url.split('?')[1].split('&'),
	data = {}, tmp;
	for (var i = 0, l = params.length; i < l; i++) 
	{
	tmp = params[i].split('=');
	data[tmp[0]] = tmp[1];
	}
	console.log(data.name);
	var url = "./files/" + data.name;
	readTextFile(url);
}

function readTextFile(file)
{
  	var rawFile = new XMLHttpRequest();
  	rawFile.open("GET", file, false);
  	rawFile.onreadystatechange = function ()
  	{
    	if(rawFile.readyState === 4)
    	{
      		if(rawFile.status === 200 || rawFile.status == 0)
      		{
      			console.log(rawFile.responseText);
        		var allText = rawFile.responseText.split(/\r\n|\n/);
        		for(var i = 0; i < allText.length; i++)
	            {
	              	var c = allText[i];
	              	b.push(c.split(','));
	            }
		        console.log(b);
      		}
    	}
  	}
  	rawFile.send(null);
  	fileCount = b.length;
  	lineCount = b[0].length;
  	ecamESDVisual();
  	ecamPDVisual();
  	ecamSDVisual();

  	pfdHDVisual();
  	pfdVSCptVisual();
}

function ecamSDVisual()
{
	for(var i = 0; i < ecamSDMne.length; i++)
	{
		for(var j = 0; j < lineCount; j++)
		{
			switch(b[0][j])
			{
				case ecamSDMne[i]: 
				for(var k = 1; k < fileCount; k++)
				{
					ecamSDVal[i].push(b[k][j]);
				}
				break;
			}
		}
	}
}

function ecamESDVisual()
{
	for(var i = 0; i < ecamESDMne.length; i++)
	{
		for(var j = 0; j < lineCount; j++)
		{
			switch(b[0][j])
			{
				case ecamESDMne[i]: 
				for(var k = 1; k < fileCount; k++)
				{
					ecamESDVal[i].push(b[k][j]);
				}
				break;
			}
		}
	}
}

function ecamPDVisual()
{
	for(var i = 0; i < ecamPDMne.length; i++)
	{
		for(var j = 0; j < lineCount; j++)
		{
			switch(b[0][j])
			{
				case ecamPDMne[i]: 
				for(var k = 1; k < fileCount; k++)
				{
					ecamPDVal[i].push(b[k][j]);
				}
				break;
			}
		}
	}
}

function pfdHDVisual()
{
	for(var i = 0; i < pfdHDMne.length; i++)
	{
		for(var j = 0; j < lineCount; j++)
		{
			switch(b[0][j])
			{
				case pfdHDMne[i]: 
				for(var k = 1; k < fileCount; k++)
				{
					pfdHDVal[i].push(b[k][j]);
				}
				break;
			}
		}
	}
}

function pfdVSCptVisual()
{
	for(var i = 0; i < pfdVSCptMne.length; i++)
	{
		for(var j = 0; j < lineCount; j++)
		{
			switch(b[0][j])
			{
				case pfdVSCptMne[i]: 
				for(var k = 1; k < fileCount; k++)
				{
					pfdVSCptVal[i].push(b[k][j]);
				}
				break;
			}
		}
	}

	console.log(pfdVSCptVal);
}

function textChange(ob, pos, col, text, size, val)
{
	if(text != undefined)
	{
		ob.setAttribute('position',pos);
		ob.setAttribute('color',col);
		ob.setAttribute('width',size);

		if(val === 0 || val === '0')
		{
			ob.setAttribute('value','');
		}

		else
		{
			ob.setAttribute('value', text);	
		}
	}
}

function animationPause(ob, animBegin, animPause, animResume)
{
	var currentEvent = animBegin;
	document.getElementById(ob).dispatchEvent(new CustomEvent(currentEvent))
  	switch(currentEvent)
  	{
	    case animBegin:
	    case animResume:
	      	currentEvent = animPause
	    break
	    case animPause:
	      	currentEvent = animResume          
	    break  
  	}
}

function changeAnimationParameters(ob, from, to)
{
	document.getElementById(ob).setAttribute('animation','from',from);
	document.getElementById(ob).setAttribute('animation','to',to);
}

function timeAnimate()
{
	if(animStatus === true)
	{
		if(i < fileCount)
		{
			setTimeout(function()
			{
				// if(temp29[i] != temp29[i-1])
				// {
					var cabAltVal = temp29[i];
					temp29[i] = parseInt(temp29[i]);
					temp29[i] = ((temp29[i]/11000)*240) - 50;
					cabAlt.setAttribute('rotation', temp29[i] + ' 90 0');
					textChange(val4SD, temp29_pos, temp29_col, cabAltVal, temp29_size, 1);
				// }

				// if(temp30[i] != temp30[i-1])
				// {
					var cabVSVal = temp30[i];
					temp30[i] = parseInt(temp30[i]);
					temp30[i] = ((temp30[i]/65536)*180);
					cabVs.setAttribute('rotation', temp30[i] + ' 90 0');
					textChange(val5SD, temp30_pos, temp30_col, cabVSVal, temp30_size, 1);
				// }

				// if(temp31[i] != temp31[i-1])
				// {
					var delPVal = temp31[i];
					temp31[i] = parseInt(temp31[i]);
					temp31[i] = ((temp31[i]/10)*180) - 50;
					delP.setAttribute('rotation', temp31[i] + ' 90 0');
					textChange(val6SD, temp31_pos, temp31_col, delPVal, temp31_size, 1);
				// }

				// if(temp8[i] != temp8[i-1])
				// {
					var egtLVal = temp8[i];
					temp8[i] = parseInt(temp8[i]);
					temp8[i] = temp8[i] + 80;
					temp8[i] = ((temp8[i]/1342)*180);
					egtL.setAttribute('rotation', temp8[i] + ' 90 0');
					textChange(val8ESD, temp8_pos, temp8_col, egtLVal, temp8_size, 1);
				// }
				// else
				// {
					// textChange(val8ESD, temp8_pos, temp8_col, egtLVal, temp8_size, 1);
				// }

				// if(temp9[i] != temp9[i-1])
				// {
					var egtRVal = temp9[i];
					temp9[i] = parseInt(temp9[i]);
					temp9[i] = temp9[i] + 80;
					temp9[i] = ((temp9[i]/1342)*180);
					egtR.setAttribute('rotation', temp9[i] + ' 90 0');
					textChange(val9ESD, temp9_pos, temp9_col, egtRVal, temp9_size, 1);
				// }
				// else
				// {
					// textChange(val9ESD, temp9_pos, temp9_col, egtRVal, temp9_size, 1);
				// }

				// if(temp10[i] != temp10[i-1])
				// {
					thrust1.setAttribute('visible','true');
					var thrust1Plane = parseInt(temp10[i]);
					thra1.setAttribute('rotation', temp10[i] + ' 90 90');
					thra1plane1.setAttribute('rotation', -(90 + thrust1Plane) + ' 270 0');
					textChange(val10ESD, temp10_pos, temp10_col, temp10[i], temp10_size, 1);
				// }

				// if(temp11[i] != temp11[i-1])
				// {
					thrust2.setAttribute('visible','true');
					var thrust2Plane = parseInt(temp11[i]);
					thra2.setAttribute('rotation', temp11[i] + ' 90 90');
					thra2plane1.setAttribute('rotation', -(90 + thrust2Plane) + ' 270 0');
					textChange(val11ESD, temp11_pos, temp11_col, temp12[i], temp11_size, 1);
				// }

				// if(temp12[i] != temp12[i-1])
				// {
					var thrustPlane1 = parseInt(temp12[i]);
					thrc1.setAttribute('rotation', temp12[i] + ' 90 90');
					thra1plane2.setAttribute('rotation', (90 + thrustPlane1) + ' 90 0');
				// }

				// if(temp13[i] != temp13[i-1])
				// {
					var thrustPlane2 = parseInt(temp13[i]);
					thrc2.setAttribute('rotation', temp13[i] + ' 90 90');
					thra2plane2.setAttribute('rotation', (90 + thrustPlane2) + ' 90 0');
				// }

				textChange(val1ESD, temp1_pos, temp1_col, 'A. Floor', temp1_size, temp1[i]);
				textChange(val2ESD, temp2_pos, temp2_col, temp2[i], temp2_size, 1);
				textChange(val3ESD, temp3_pos, temp3_col, 'NAI', temp3_size, temp3[i]);
				textChange(val4ESD, temp4_pos, temp4_col, 'WAI', temp4_size, temp4[i]);
				textChange(val5ESD, temp5_pos, temp5_col, 'PACKS', temp5_size, temp5[i]);
				textChange(val6ESD, temp6_pos, temp6_col, 'IDLE', temp6_size, temp6[i]);
				textChange(val7ESD, temp7_pos, temp7_col, temp7[i], temp7_size, 1);
				textChange(val14ESD, temp14_pos, temp14_col, temp14[i], temp14_size, 1);
				textChange(val15ESD, temp15_pos, temp15_col, temp15[i], temp15_size, 1);

				textChange(val1PD, temp19_pos, temp19_col, temp19[i], temp19_size, 1);
				textChange(val2PD, temp20_pos, temp20_col, temp20[i], temp20_size, 1);
				textChange(val3PD, temp21_pos, temp21_col, temp21[i], temp21_size, 1);
				textChange(val4PD, temp22_pos, temp22_col, temp22[i], temp22_size, 1);
				textChange(val5PD, temp23_pos, temp23_col, temp23[i], temp23_size, 1);
				textChange(val6PD, temp24_pos, temp24_col, temp24[i], temp24_size, 1);
				textChange(val7PD, temp25_pos, temp25_col, temp25[i], temp25_size, 1);

				textChange(val1SD, temp26_pos, temp26_col, temp26[i], temp26_size, 1);
				textChange(val2SD, temp27_pos, temp27_col, temp27[i], temp27_size, 1);
				textChange(val3SD, temp28_pos, temp28_col, '1998', temp28_size, 1);
				textChange(val7SD, temp32_pos, temp32_col, temp32[i], temp32_size, 1);
				textChange(val8SD, temp33_pos, temp33_col, temp33[i], temp33_size, 1);

				if(temp51[i] != undefined)
				{
					var a = temp51[i].split(' ');
					temp51[i] = a[0];
					temp52[i] = a[1];
				}

				i++;
				console.log(i);
				timeAnimate();
			}, timeVal);
		}
	}
}

document.addEventListener('click',function()
{
	if(animStatus === false)
	{
		animStatus = true;
		var a = timeAnimate();
	}
	else
	{
		animStatus = false;
	}
});